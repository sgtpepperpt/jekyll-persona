# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "persona-jekyll"
  spec.version       = "0.2.0"
  spec.authors       = ["Guilherme Borges"]
  spec.email         = ["g.borges@campus.fct.unl.pt"]

  spec.summary       = "A simple personal page theme for Jekyll."
  spec.homepage      = "https://gitlab.com/sgtpepperpt/jekyll-persona"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.8"

  spec.add_development_dependency "bundler", "~> 2.0.1"
  spec.add_development_dependency "rake", "~> 12.0"
end
